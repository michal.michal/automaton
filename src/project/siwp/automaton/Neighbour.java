package project.siwp.automaton;

public class Neighbour
{
    Node node;
    int transitionLabel;

    public Neighbour(Node neighbour, int transitionLabel)
    {
        this.node = neighbour;
        this.transitionLabel = transitionLabel;
    }
}
