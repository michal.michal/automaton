package project.siwp.automaton;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;

public class Network
{
    LinkedList<Automaton> net;
    int nTransitions;
    Integer transitionLabels[];
    Integer nodeLabels[];

    public Network()
    {
        this.net = new LinkedList<>();
    }

    public void addAutomaton(Automaton newAutomaton)
    {
        this.net.add(newAutomaton);
    }

    public void countTransitions()
    {
        HashSet<Integer> uniqueTransitions = new HashSet<>();
        for (Automaton a : this.net)
        {
            for (Node n : a.nodes)
            {
                for(Neighbour ne : n.neighbours)
                {
                    uniqueTransitions.add(ne.transitionLabel);
                }
            }
        }

        this.nTransitions = uniqueTransitions.size();
        this.transitionLabels = uniqueTransitions.toArray(new Integer[uniqueTransitions.size()]);
    }

    public void countNodes()
    {
        HashSet<Integer> uniqueNodes = new HashSet<>();
        for(Automaton a : this.net)
        {
            for(Node n : a.nodes)
            {
                uniqueNodes.add(n.label);
            }
        }

        this.nodeLabels = uniqueNodes.toArray(new Integer[uniqueNodes.size()]);
    }

    public boolean checkTransition(int transitionLabel)
    {
        for(Automaton automaton : net)
        {
            if(automaton.transitionExists(transitionLabel))
            {
                if(!automaton.isTransitionAdjToCurrent(transitionLabel))
                    return false;
            }
        }

        return true;
    }

    public void makeTransition(int transitionLabel)
    {
        for (Automaton automaton : this.net)
        {
            automaton.makeTransition(transitionLabel);
        }
    }

    public void setState(ArrayList<Integer> state)
    {
        int i = 0;
        for(Automaton a : this.net)
        {
            a.setCurrent(state.get(i++));
        }
    }

    public ArrayList<Integer> getState()
    {
        ArrayList<Integer> state = new ArrayList<>();
        for(Automaton a : this.net)
        {
            state.add(a.current.label);
        }

        return state;
    }

    public ProductAutomaton solve()
    {
        ArrayList<Integer> state;
        Stack<ArrayList<Integer>> stateStack = new Stack<>();
        ProductAutomaton productAutomaton = new ProductAutomaton(this);

        //initial state
        state = this.getState();

        productAutomaton.add(state);
        stateStack.push(state);

        LinkedList<Integer> transitionQueue = new LinkedList<>();
        int transition;
        ArrayList<Integer> newState;
        while(!stateStack.empty())
        {
            state = stateStack.pop();
            this.setState(state);
            for(Automaton a : this.net)
            {
                for(Neighbour n : a.current.neighbours)
                {
                    if(!transitionQueue.contains(n.transitionLabel))
                    {
                        transitionQueue.addLast(n.transitionLabel);
                    }
                }
            }
            while(!transitionQueue.isEmpty())
            {
                transition = transitionQueue.poll();
                if(this.checkTransition(transition))
                {
                    this.makeTransition(transition);
                    newState = this.getState();
                    if(productAutomaton.contains(newState))
                    {
                        productAutomaton.addTransition(state, newState, transition);
                    }
                    else
                    {
                        stateStack.push(newState);
                        productAutomaton.add(newState);
                        productAutomaton.addTransition(state, newState, transition);
                    }
                    this.setState(state); // state before transition
                }
            }
        }

        return productAutomaton;
    }

    public LinkedList<Automaton> getNet() {return this.net;}

    public Integer[] getTransitionLabels()
    {
        return transitionLabels;
    }

    public Integer[] getNodeLabels()
    {
        return nodeLabels;
    }
}
