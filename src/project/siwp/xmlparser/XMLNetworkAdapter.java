package project.siwp.xmlparser;

import project.siwp.automaton.*;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;

class XMLAutomatonAdapter {
    @XmlAttribute(name = "id")
    int id;
    @XmlAttribute(name = "location-from")
    int locationFrom;
    @XmlAttribute(name = "location-to")
    int locationTo;

    @XmlElements(@XmlElement(name = "transition"))
    ArrayList<XMLTransitionAdapter> transitions;

    public XMLAutomatonAdapter() {
        transitions = new ArrayList<>();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\tAUTOMATON_" + this.id + "\n\t\tLOCATION[" + this.locationFrom + ":" + this.locationTo + "]\n\t\tTRANSITIONS\n");

        for (XMLTransitionAdapter t : transitions) {
            stringBuilder.append(t.toString());
        }

        stringBuilder.append("\tEND\n");

        return stringBuilder.toString();
    }

}

class XMLTransitionAdapter {
    @XmlAttribute(name = "start")
    int start;
    @XmlAttribute(name = "end")
    int end;
    @XmlAttribute(name = "label")
    int label;

    public XMLTransitionAdapter() {}

    public XMLTransitionAdapter(int s, int e, int l) {
        start = s;
        end = e;
        label = l;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\t\t" + this.start + "_" + this.end + "_" + this.label + "\n");

        return stringBuilder.toString();
    }

}

@XmlRootElement(name = "network")
public class XMLNetworkAdapter {
    @XmlElements(@XmlElement(name = "automaton"))
    ArrayList<XMLAutomatonAdapter> automata;

    public XMLNetworkAdapter() {
        automata = new ArrayList<>();
    }

    public XMLNetworkAdapter(Network n) {
        int k = 0;
        automata = new ArrayList<>();

        for (Automaton a : n.getNet()) {
            XMLAutomatonAdapter automaton = new XMLAutomatonAdapter();
            automaton.id = ++k;
            automaton.locationFrom = a.getStart();
            automaton.locationTo = a.getEnd();
            automaton.transitions = new ArrayList<>();

            for (Node nd : a.getNodes()) {
                for (int[] i : nd.getTransitions()) {
                    automaton.transitions.add(new XMLTransitionAdapter(i[0], i[1], i[2]));
                }
            }

            automata.add(automaton);
        }
    }

    public Network ToNetwork() {
        Network n = new Network();

        for (XMLAutomatonAdapter automaton : this.automata) {
            Automaton a = new Automaton(automaton.locationFrom, automaton.locationTo);

            for (XMLTransitionAdapter tranistion : automaton.transitions) {
                a.addTransition(tranistion.start, tranistion.end, tranistion.label);
            }

            a.setCurrent(automaton.locationFrom);
            a.setNodeInitial(automaton.locationFrom);
            a.setNodeFinal(automaton.locationTo);

            n.addAutomaton(a);
        }

        return n;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("NETWORK\n");

        for (XMLAutomatonAdapter a : automata) {
            stringBuilder.append(a.toString());
        }

        stringBuilder.append("END\n");

        return stringBuilder.toString();
    }

}
